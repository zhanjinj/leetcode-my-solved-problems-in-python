# Linked List

🌟 **21. Merge Two Sorted Lists:** [problem](https://leetcode.com/problems/merge-two-sorted-lists/) 📗

- [solution](./LinkedList/21-Merge-Two-Sorted-Lists.py) (Runtime: 32ms, 87.94%; Memory: 13.9MB)



🌟 **141. Linked List Cycle:** [problem](https://leetcode.com/problems/linked-list-cycle/) 📗

- [solution1](./LinkedList/141-Linked-List-Cycle-1.py) (Runtime: 56ms, 15.74%; Memory: 16.8MB)
- [solution2: fast and slow pointers](./LinkedList/141-Linked-List-Cycle-2.py) (Runtime: 40ms, 96.09%; Memory: 17MB)



🌟 **142. Linked List Cycle II:** [problem](https://leetcode.com/problems/linked-list-cycle-ii/) 📗

- [solution: fast and slow pointers](./LinkedList/141-Linked-List-Cycle-II.py) (Runtime: 48ms, 78.11%; Memory: 16.9MB)



🌟 **160. Intersection of Two Linked Lists:** [problem](https://leetcode.com/problems/intersection-of-two-linked-lists/) 📙

- [solution1](./LinkedList/160-Intersection-of-Two-Linked-Lists-1.py) (Runtime: 156ms, 95.53%; Memory: 29.2MB)
- [solution2](./LinkedList/160-Intersection-of-Two-Linked-Lists-2.py) (Runtime: 164ms, 79.79%; Memory: 29.MB)



🌟 **206. Reverse Linked List:** [problem](https://leetcode.com/problems/reverse-linked-list/) 📗

- [solution](./LinkedList/206-Reverse-Linked-List.py) (Runtime: 32ms, 82.86%; Memory: 15.2MB, 31.82%)



🌟 **876. Middle of the Linked List:** [problem](https://leetcode.com/problems/middle-of-the-linked-list/) 📗

- [solution](./LinkedList/876-Middle-of-the-Linked-List.py) (Runtime: 32ms, 36.67%; Memory: 13.9MB)