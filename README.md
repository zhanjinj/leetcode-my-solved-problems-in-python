# Leetcode : my solved problems in python

In order to record all of my problem solving methods in leetcode, a question will be optimized in different ways and will not cover the original answer. I hope I can stick to the algorithm practice (´･_･`)



📗: Easy

📙: Medium

📕: Hard



### [List](./List/List.md)

### [Linked List](./LinkedList/LinkedList.md)

### [Tree](./Tree/Tree.md)

### [Dynamic Programming](./DynamicProgramming/DynamicProgramming.md)

