# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

class Solution:
    def levelOrder(self, root: TreeNode) -> List[List[int]]:
        def traversal(root, level, result):
            if not root:
                return
            result[level-1].append(root.val)
            traversal(root.left, level+1, result)
            traversal(root.right, level+1, result)
        
        from collections import defaultdict
        result = defaultdict(list)
        traversal(root, 1, result)
        return [r for r in result.values()]