# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

class Solution:
    def preorderTraversal(self, root: TreeNode) -> List[int]:
        def preorder(root, result):
            if not root:
                return
            result.append(root.val)
            preorder(root.left, result)
            preorder(root.right, result)
        l = []
        if not root:
            return l
        preorder(root, l)
        return l