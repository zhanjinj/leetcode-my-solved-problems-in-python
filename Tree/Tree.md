# Tree

🌟 **102. Binary Tree Level Order Traversal:** [problem](https://leetcode.com/problems/binary-tree-level-order-traversal/) 📙

- [solution1: iterative](./102-binary-tree-level-order-traversal-1.py) (Runtime: 32ms, 75.00%, 14.4MB)
- [solution2: non iterative](./102-binary-tree-level-order-traversal-2.py) (Runtime: 32ms, 75.00%, 14.3MB)




🌟 **144. Binary Tree Preorder Traversal:** [problem](https://leetcode.com/problems/binary-tree-preorder-traversal/) 📙

- [solution](./144-binary-tree-preorder-traversal.py) (Runtime: 32ms, 34.26%; Memory: 13.9MB)




🌟 **226. Invert Binary Tree:** [problem](https://leetcode.com/problems/invert-binary-tree/) 📗

- [solution](./226-invert-binary-tree.py) (Runtime: 20ms, 97.73%; Memory: 13.9MB)




🌟 **572. Subtree of Another Tree:** [problem](https://leetcode.com/problems/subtree-of-another-tree/) 📗

- [solution](./572-subtree-of-another-tree.py) (Runtime: 236ms, 76.23%; Memory: 15MB)

