# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

class Solution:
    def isSubtree(self, s: TreeNode, t: TreeNode) -> bool:
        def isSameTree(s, t):
            if not s and not t:
                return True
            if not s or not t:
                return False
            if s.val != t.val:
                return False
            return isSameTree(s.left, t.left) and isSameTree(s.right, t.right)
        def isSubtreee(s, t):
            if not s:
                return False
            if isSameTree(s, t):
                return True
            return isSubtreee(s.left, t) or isSubtreee(s.right, t)
        return isSubtreee(s, t)