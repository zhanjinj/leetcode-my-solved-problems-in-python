# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

class Solution:
    def invertTree(self, root: TreeNode) -> TreeNode:
        def invertTreee(root):
            tmp = root.left
            root.left = root.right
            root.right = tmp
            if root.left:
                invertTreee(root.left)
            if root.right:
                invertTreee(root.right)
            return root
        if not root:
            return root
        return invertTreee(root)