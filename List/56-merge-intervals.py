class Solution:
    def merge(self, intervals: List[List[int]]) -> List[List[int]]:
        length = len(intervals)
        if length > 0:
            intervals = sorted(intervals)
            returnIntervals = [intervals[0]]
            for i in range(1, length):
                r = returnIntervals.pop()
                if intervals[i][0] > r[1]:
                    returnIntervals.append(r)
                    returnIntervals.append(intervals[i])
                else:
                    returnIntervals.append([r[0], max(r[1], intervals[i][1])])
            return returnIntervals
        else:
            return []