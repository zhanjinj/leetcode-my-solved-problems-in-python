class Solution:
    def minSubArrayLen(self, s: int, nums: List[int]) -> int:
        if sum(nums) < s:
            return 0
        else:
            summ = 0
            length = len(nums)
            head = 0
            for i in range(length):
                summ = summ + nums[i]
                while summ>=s:
                    length = min(i - head + 1, length)
                    summ = summ-nums[head]
                    head = head + 1
        return length