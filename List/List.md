# List

🌟 **56. Merge Intervals:** [problem](https://leetcode.com/problems/merge-intervals/) 📙

- [solution](./56-merge-intervals.py) (Runtime: 80ms, 96.59%; Memory: 15.7MB, 6.52%)



🌟 **57. Insert Interval:** [problem](https://leetcode.com/problems/insert-interval/) 📕

- [solution](./57-insert-interval.py) (Runtime: 84ms, 52.84%; Memory: 17.1MB, 8.00%)



🌟 **75. Sort Colors: ** [problem](https://leetcode.com/problems/sort-colors/) 📙

- [solution](./75-sort-colors.py) (Runtime: 24ms, 97.71%; Memory: 13.7MB, 6.25%)



🌟 **209. Minimum Size Subarray Sum: **[problem](https://leetcode.com/problems/minimum-size-subarray-sum/) 📙

- [solution: sliding window](./209-minimum-size-subarray-sum.py) (Runtime: 68ms, 95.53%; Memory: 16.4MB, 7.69%)



🌟 **215. Kth Largest Element in an Array:** [problem](https://leetcode.com/problems/kth-largest-element-in-an-array/) 📙

- [solution1: min heap realised by list](./215-kth-largest-element-in-an-array-1.py) (Runtime: 132ms, 20.40%; Memory: 14.5MB, 10.00%)

- [solution2: min heap realised by heapq](./215-kth-largest-element-in-an-array-2.py) (Runtime: 60ms, 92.18%; Memory: 14.5MB, 10.00%)



🌟 **347. Top K Frequent Elements:** [problem](https://leetcode.com/problems/top-k-frequent-elements/) 📙

- [solution1: dict & min heap](./347-top-k-frequent-elements-1.py) (Runtime: 100ms, 89.10%; Memory: 18.3MB, 6.25%)
- [solution2: collections.Counter().most_common()](./347-top-k-frequent-elements-2.py) (Runtime: 96ms, 96.38%; Memory: 18.3MB, 6.25%)

