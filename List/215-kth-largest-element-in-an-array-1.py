class Solution:
    def findKthLargest(self, nums: List[int], k: int) -> int:
        min_heap = [float("-inf")] * k
        for item in nums:
            if item > min_heap[0]:
                min_heap[0] = item
                i = 0
                while 2*i+2 < k:
                    if min_heap[2*i+1] <= min_heap[2*i+2] and min_heap[2*i+1] < min_heap[i]:
                        min_heap[2*i+1], min_heap[i] = min_heap[i], min_heap[2*i+1]
                        i = 2*i+1
                    elif min_heap[2*i+1] > min_heap[2*i+2] and min_heap[2*i+2] < min_heap[i]:
                        min_heap[2*i+2], min_heap[i] = min_heap[i], min_heap[2*i+2]
                        i = 2*i+2
                    else:
                        break
                if 2*i+1 < k and min_heap[2*i+1] < min_heap[i]:
                    min_heap[2*i+1], min_heap[i] = min_heap[i], min_heap[2*i+1]
        return min_heap[0]