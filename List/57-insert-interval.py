class Solution:
    def insert(self, intervals: List[List[int]], newInterval: List[int]) -> List[List[int]]:
        length = len(intervals)
        if length > 0:
            if newInterval[0] > intervals[length-1][1]:
                return intervals + [newInterval]
            if newInterval[1] < intervals[0][0]:
                return [newInterval] + intervals
            for i in range(length):
                if intervals[i][1] >= newInterval[0]:
                    start = i
                    break
            for i in range(length-1, -1, -1):
                if intervals[i][0] <= newInterval[1]:
                    end = i
                    break
            if start <= end:
                newInterval[0] = min(intervals[start][0], newInterval[0])
                newInterval[1] = max(intervals[end][1], newInterval[1])
            return intervals[0:start] + [newInterval] + intervals[end+1:]
        else:
            return [newInterval]