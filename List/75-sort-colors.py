class Solution:
    def sortColors(self, nums: List[int]) -> None:
        """
        Do not return anything, modify nums in-place instead.
        """
        right = len(nums)-1
        left = 0
        while left<=right and nums[left] == 0:
            left += 1
        while right>=0 and nums[right] == 2:
            right -= 1
        j = left
        while j <= right:
            if nums[j] == 2:
                nums[j], nums[right] = nums[right], nums[j]
                right -= 1
            if nums[j] == 1:
                j += 1
                continue
            if nums[j] == 0:
                nums[j], nums[left] = nums[left], nums[j]
                j += 1
                left += 1
                continue