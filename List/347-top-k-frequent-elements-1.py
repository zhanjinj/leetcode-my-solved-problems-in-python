class Solution:
    def topKFrequent(self, nums: List[int], k: int) -> List[int]:
        freq = dict()
        for item in nums:
            if item in freq.keys():
                freq[item] += 1
            else:
                freq[item] = 1
        min_heap = [[float("-inf"), float("-inf")]] * k
        heapq.heapify(min_heap)
        for item in freq.keys():
            if freq[item] > min_heap[0][0]:
                heapq.heappushpop(min_heap, [freq[item], item])
        return [i[1] for i in min_heap]