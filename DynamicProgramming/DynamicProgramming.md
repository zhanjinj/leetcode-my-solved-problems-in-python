# Dynamic programming

🌟 **53. Maximum Subarray:** [problem](https://leetcode.com/problems/maximum-subarray/) 📗

- [solution](./DynamicProgramming/53-maximum-subarray.py) (Runtime: 64ms, 81.13%; Memory: 14.7MB)



🌟 **70. Climbing Stairs:** [problem](https://leetcode.com/problems/climbing-stairs/) 📗

- [solution](./DynamicProgramming/70-climbing-stairs.py) (Runtime: 28ms, 65.41%; Memory: 13.9MB)



🌟 **198. House Robber:** [problem](https://leetcode.com/problems/house-robber/) 📗

- [solution](./DynamicProgramming/198-house-robber.py) (Runtime: 32ms, 43.21%; Memory: 13.8MB)