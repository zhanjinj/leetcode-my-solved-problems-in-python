class Solution:
    def rob(self, nums: List[int]) -> int:
        if len(nums) == 0:
            return 0
        dp0 = 0
        dp1 = nums[0]
        max_value = dp1
        for i in range(1, len(nums)):
            dp2 = max(dp0+nums[i], dp1)
            dp0 = dp1
            dp1 = dp2
            max_value = max(max_value, dp2)
        return max_value