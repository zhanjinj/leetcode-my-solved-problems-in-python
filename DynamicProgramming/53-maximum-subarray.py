class Solution:
    def maxSubArray(self, nums: List[int]) -> int:
        dp_prev = nums[0]
        max_value = dp_prev
        for i in range(1, len(nums)):
            dp_prev = max(nums[i], nums[i]+dp_prev)
            max_value = max(max_value, dp_prev)
        return max_value